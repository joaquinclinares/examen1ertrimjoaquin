<?php 
    include_once 'UsuariosCreados.php';
    include_once 'UsuariosClass.php';

    $usuario1=new UsuariosClass('frodo@gmail.com','elAnillo');
    $usuario2=new UsuariosClass('gandalf@gmail.com','soyMago');

    $almacenUsuarios=new UsuariosCreados();

    $almacenUsuarios->anyadeUsuario($usuario1);
    $almacenUsuarios->anyadeUsuario($usuario2);

    if($_SERVER['REQUEST_METHOD']==='POST'){
        extract($_POST);

        if(!empty($nombreUsuario) && !empty($contrasenya)){
            $existe=false;
            $existe=($almacenUsuarios->compruebaExiste($nombreUsuario,$contrasenya));
            if($existe==true)
                muestraLogExito();
            else
                muestraLogFail();

        }
        else{
            echo"<p style=\"color:red\"><strong>No has rellenado alguno de los campos obligatorios</strong></p>";
            
            muestraFormulario($nombreUsuario,$contrasenya);
        }
    }else{
        muestraFormulario();
    }
     function muestraFormulario($nombreUsuario="", $contrasenya="") {

     ?>
     <form name="login" action="<?= $_SERVER['PHP_SELF'] ?>" method="post">
         <fieldset>
             <legend>Login AnunciosClasificados</legend>
                 <br/>
             <label for="nombreUsuario">Nombre de usuario o email* : </label>
             <input type="text" name="nombreUsuario" value="<?= $nombreUsuario ?>" required />
                 <br/>
             <label for="precioMax">Contrasenya*: </label>
             <input type="text" name="contrasenya" value="<?= $contrasenya ?>" required />
                 <br/>
             <br/><br/>
             <button type="submit" name="enviar">Envia</button>
             <br/>
         </fieldset>
     </form>

<?php
    }
    function muestraLogExito(){
        echo('Loggeado en la web con exito');
    }
    function muestraLogFail(){
        echo('No has introducido bien alguno de los campos, intentalo de nuevo');
        muestraFormulario();
    }

?>