<?php
    include_once 'anunciosClass.php';
    class GuardaAnuncios extends AnunciosClass{

        private static $arrayAnuncios=array();

        public function __construct(){
            
        }
        public function __toString(){
            $contenido="";
            foreach (self::$arrayAnuncios as $anun){
                $contenido=$contenido.$anun->__toString();
            } 
            return $contenido;
        }
        // public function mostrarAnuncios(){
        //     foreach (self::$arrayAnuncios as $anun){
        //         $anun->__toString();
        //     }
        // }
        public function anyadeAnuncio($nuevoAnuncio){
            array_push(self::$arrayAnuncios,$nuevoAnuncio);
        }

    }

?>