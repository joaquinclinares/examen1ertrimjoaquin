<?php 

    class Oferta{
        private $fechaCreacion;
        private $usuario;
        private $textOferta;
        private $preciOferta;

        public function __construct(){

        }
        public function __toString(){

        }

        public function getFechaCreacion(){
                return $this->fechaCreacion;
        }

        public function setFechaCreacion($fechaCreacion){
                $this->fechaCreacion = $fechaCreacion;

                return $this;
        }

        public function getUsuario(){
                return $this->usuario;
        }
        public function setUsuario($usuario){
                $this->usuario = $usuario;

                return $this;
        }

        public function getTextOferta(){
                return $this->textOferta;
        }

        public function setTextOferta($textOferta) {
                $this->textOferta = $textOferta;

                return $this;
        }

        public function getPreciOferta(){
                return $this->preciOferta;
        }

        public function setPreciOferta($preciOferta){
                $this->preciOferta = $preciOferta;

                return $this;
        }
    }




?>