<?php 

    class UsuariosClass{
        private static $contador=0;
        private $id;
        private $userEmail;
        private $contrasenya;

        public function __constructorr($userEmail,$contrasenya){
            incrementaContador();
            $this->userEmail=$userEmail;
            $this->contrasenya=$contrasenya;
            $this->id=self::$contador;
        }
        public function __toString(){

        }
        public function incrementaContador(){
            self::$contador+=1;

        }
        public function getId(){
                return $this->id;
        }
        public function setId($id){
                $this->id = $id;
                return $this;
        }
        public function getUserEmail(){
                return $this->userEmail;
        }
        public function setUserEmail($userEmail){
                $this->userEmail = $userEmail;

                return $this;
        }
        public function getContrasenya(){
                return $this->contrasenya;
        }
        public function setContrasenya($contrasenya){
                $this->contrasenya = $contrasenya;

                return $this;
        }
    }


?>