<?php
    class AnunciosClass{
        private static $arrayAnuncios;

        private $titulo;
        private $fechaCreacion;
        private $usuario;
        private $cantidadOfrs;
        private $textoAmpliadoAn;
        private $precioArticulo;
        private $imagenArticulo;
        private $enlaceVer;
        private $stock;
        
        public function __construct($datosAnuncio){
            $this->titulo=$datosAnuncio['titulo'];
            $this->textoAmpliadoAn=$datosAnuncio['descripcion'];
            $this->precioArticulo=$datosAnuncio['precio'];
            $this->stock=$datosAnuncio['stock'];

            $this->fechaCreacion=date('l jS \of F Y h:i:s A');
            $this->cantidadOfrs=0;
            $this->imagenArticulo="";
            $this->enlaceVer="";

            //$this->=$datosAnuncio[''];
        }
        public function __toString(){
            return '<p>'.   'Titulo anuncio: '.$this->titulo.'<br/>'.
                            'Creado el : '.$this->fechaCreacion.'<br/>'.
                            'Por el usuario: '.$this->usuario.'<br/>'.
                            'Cantidad ofertas disponibles: '.$this->cantidadOfrs.'<br/>'.
                            'Descripcion: '.$this->textoAmpliadoAn.'<br/>'.
                            'Precio articulo: '.$this->precioArticulo.'<br/>'.
                            'UrlImagen: '.$this->imagenArticulo.'<br/>'.
                            'Enlace al anuncio: '.$this->creaEnlace().'<br/>'.'<br/>'.
                    '</p>';
        }
        public function creaEnlace(){
                $url='AnuncioConcreto.php?
                titulo='.$this->titulo.
                '&creado_el='.$this->fechaCreacion.
                '&cantidad='.$this->cantidadOfrs.
                '&descripcion='.$this->textoAmpliadoAn.
                '&precio='.$this->precioArticulo.
                '&imagen='.$this->imagenArticulo;
                return '<a href="'.$url.'">Ver anuncio</a>'.'</li>';
        }
   
        public function getTitulo(){
                return $this->titulo;
        }
        public function setTitulo($titulo){
                $this->titulo = $titulo;
         
                return $this;
        }
      public function getFechaCreacion(){
                return $this->fechaCreacion;
        }

        public function setFechaCreacion($fechaCreacion)
        {
                $this->fechaCreacion = $fechaCreacion;

                return $this;
        }
        public function getUsuario()
        {
                return $this->usuario;
        }
        public function setUsuario($usuario){
                $this->usuario = $usuario;

                return $this;
        }
        public function getCantidadOfrs(){
                return $this->cantidadOfrs;
        }

        public function setCantidadOfrs($cantidadOfrs){
                $this->cantidadOfrs = $cantidadOfrs;

                return $this;
        }
        public function getTextoAmpliadoAn(){
                return $this->textoAmpliadoAn;
        }

        public function setTextoAmpliadoAn($textoAmpliadoAn){
                $this->textoAmpliadoAn = $textoAmpliadoAn;

                return $this;
        }
        public function getPrecioArticulo(){
                return $this->precioArticulo;

        }
        public function setPrecioArticulo($precioArticulo)
        {
                $this->precioArticulo = $precioArticulo;

                return $this;
        }
        public function getImagenArticulo(){
                return $this->imagenArticulo;
        }

        public function setImagenArticulo($imagenArticulo){
                $this->imagenArticulo = $imagenArticulo;

                return $this;
        }
        public function getEnlaceVer(){
                return $this->enlaceVer;
        }

        public function setEnlaceVer($enlaceVer){
                $this->enlaceVer = $enlaceVer;

                return $this;
        }
        public function getStock(){
                return $this->stock;
        }
        public function setStock($stock)
        {
                $this->stock = $stock;

                return $this;
        }
}




?>